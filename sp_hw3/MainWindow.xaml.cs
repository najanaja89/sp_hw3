﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Media;
using Microsoft.Win32;
using System.IO;
using System.ComponentModel;
using System.Threading;

namespace sp_hw3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var threadPlayer = new Thread(MusicPlayer);
            threadPlayer.IsBackground = true;
            threadPlayer.Start();


            //Console.WriteLine("Main Thread end work");
        }


        private void WindowClosing(object sender, CancelEventArgs e)
        {
            var threadText = new Thread(SaveFileClosing);
            threadText.Start(new SavedText { Text = richTextBox.Text });
        }

        private void MusicPlayer()
        {
            var player = new MediaPlayer();
            player.Open(new Uri(AppDomain.CurrentDomain.BaseDirectory + "background.mp3", UriKind.RelativeOrAbsolute));
            while (true)
            {
                player.Play();
            }
        }

        private void SaveFileClosing(object text)
        {
            string writePath = @"LastText.txt";
            using (StreamWriter writer = new StreamWriter(writePath, false, System.Text.Encoding.Default))
            {
                writer.WriteLine((text as SavedText).Text);
            }
            MessageBox.Show("File Saved");
        }

    }
}
